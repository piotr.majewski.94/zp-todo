import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { TasksService } from '../services/tasks.service';
import { Task } from '../models/Task';

@Component({
  selector: 'app-todo-task',
  templateUrl: './todo-task.component.html',
  styleUrls: ['./todo-task.component.css']
})
export class TodoTaskComponent implements OnInit {

  taskList: Array<Task> = [];

  constructor(private tasksService: TasksService) {
    tasksService.getTasksListObs().subscribe((i: Array<Task>) => {
      // slice jest dodane aby pipe sortName wiedział że zminiła się referencja i należy na nowo go przez niego przepucić
      // slice zwróciło mi nową listę
      // this.taskList = i.slice();

      this.taskList = i.filter(task => task.isDone === false);
    });
  }

  ngOnInit() { }

  done(task: Task) {
    task.isDone = true;
    this.tasksService.done(task);
  }

  remove(task: Task) {
    this.tasksService.remove(task);
  }

  saveToDB() {
    this.tasksService.saveToDB();
  }
}
