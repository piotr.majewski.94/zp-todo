import { AuthService } from './../auth/auth.service';
import { HttpService } from './../services/http.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { TasksService } from '../services/tasks.service';
import { Task } from '../models/Task';
import { FormGroup, FormArray, FormControl, Validators } from '../../../node_modules/@angular/forms';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  addForm: FormGroup;

  constructor(private tasksService: TasksService, private authService: AuthService) { }

  ngOnInit() {
    this.addForm = this.initForm();
  }

  initForm(): FormGroup {
    return new FormGroup({
      taskName: new FormArray([new FormControl(null, Validators.required)])
    });
  }

  createTaskList(): Array<Task> {
    const taskList = new Array<Task>();
    const values = <[string]>this.addForm.get('taskName').value;
    values.forEach(taskName => {
      const task = {name: taskName, userId: this.authService.user.uid, created: new Date().toLocaleDateString(), isDone: false};
      taskList.push(task);
    });
    return taskList;
  }

  onSubmit() {
    const tasksList = this.createTaskList();
    tasksList.forEach(i => {
      this.tasksService.add(i);
    });
    this.addForm = this.initForm();
  }

  addField() {
    const arr = <FormArray>this.addForm.get('taskName');
    arr.push(new FormControl(null, Validators.required));
  }
}
